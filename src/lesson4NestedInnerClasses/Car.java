package lesson4NestedInnerClasses;

public class Car {
    private final String carBrand;
    private int speed;
    private final int weight;
    private Engine engine;
    private SteeringWheel steeringWheel;


    public Car(String carBrand, int horsePower , int weight){
        this.engine = new Engine("engineBrand1", horsePower);
        this.carBrand = carBrand;
        this.weight = weight;
    }

    public class Engine {
        private int horsePower;
        private String engineBrand;
        boolean isStarted;

        public Engine(String engineBrand, int horsePower) {
            this.horsePower = horsePower;
            this.engineBrand = engineBrand;
        }

        public void start() {
            isStarted = true;

            class ThrottleValve {
                private int throttlePosition = 10;
                public void throttle(int throttlePosition) {
                    this.throttlePosition = throttlePosition;
                }
            }

            ThrottleValve throttleValve = new ThrottleValve();
            throttleValve.throttle(20);
            System.out.println("Engine started " + "заслонка установлена в позицию  " +
                    throttleValve.throttlePosition );
        }

        public void stop() {
            isStarted = false;
            System.out.println("Engine stopped");
        }

        public boolean isStarted()  {
            return isStarted;
        }

        @Override
        public String toString() {
            return "Engine{" +
                    "horsePower=" + horsePower +
                    ", engineBrand='" + engineBrand + '\'' +
                    '}';
        }
    }

    public class  SteeringWheel {
        private int angle;

        public void tureSteeringWheel(int angle)  {
            steeringWheel.angle = angle;
            if (angle > 0) {
                System.out.println("автомобиль движется правее");
            }
            else System.out.println("автомобиль движется левее");
        }
    }


    public void startMoving(int setSpeed) {
        class CheckSystems {
            public boolean check() {
                System.out.println("Checking systems....");
                if (steeringWheel == null) {
                    System.out.println("авто не может ехать, нету рулевого колеса");
                    return false;
                }
                if (speed != 0) {
                    System.out.println("Car is already moving");
                    return false;
                }
                return true;
            }
        }

        CheckSystems checkSystems = new CheckSystems();
        if (checkSystems.check()) {
            this.speed = setSpeed;
            engine.start();
            System.out.println("Car is moving, speed = " + speed);

        }
    }

    public Car.Engine getEngine() {
        return engine;
    }

    public void setEngine(String engineBrand, int horsePower) {
        engine = new Engine(engineBrand, horsePower);
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }


    public void turnLeft(int angle)  {
        steeringWheel.tureSteeringWheel(-angle);
    }
    public void turnRight(int angle)  {
        steeringWheel.tureSteeringWheel(angle);
    }

    public void addSteeringWheel() {
        steeringWheel = new SteeringWheel();
        steeringWheel.angle = 0;
        System.out.println("Рулевое колесо установлено на автомобиль");
    }


    @Override
    public String toString() {
        return "Car{" +
                "carBrand='" + carBrand + '\'' +
                ", speed=" + speed +
                ", weight=" + weight +
                ", engine=" + engine +
                '}';
    }
}
