package lesson4NestedInnerClasses;

public class Solution4 {

    public static void main(String[] args) {
        Car car1 = new Car("Challenger", 540, 2500);
        car1.addSteeringWheel();
        car1.startMoving(50);
        car1.turnLeft(10);
        car1.turnRight(10);
        System.out.println(car1.toString());

        Car.Engine newEngine = car1. new Engine("Man", 433);
        car1.setEngine(newEngine);
        System.out.println(car1.getEngine().toString());

    }
}
