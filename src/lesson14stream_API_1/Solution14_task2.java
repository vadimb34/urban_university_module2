package lesson14stream_API_1;

import java.util.ArrayList;
import java.util.List;

public class Solution14_task2 {

    public static void main(String[] args) {
        System.out.println("весь список людей");
        List<Person> personList = personGenerator(15);
        personList.forEach(System.out::println);
        System.out.println("\nсписок людей с возрастом от 30 лет");
        filterListPerson(personList, 30).forEach(System.out::println);
    }

    public static List<Person> filterListPerson(List<Person> personList, int minAge) {
        return personList.stream()
                .filter(person -> person.getAge() > minAge)
                .toList();
    }

    public static List<Person> personGenerator(int count) {
        List<Person> result = new ArrayList<>();
        int age;
        for (int i = 1; i <= count; i++) {
            age  = (int) (Math.random() * 40) + 20;
            result.add(new Person("Name" + i, age));
        }
        return result;
    }
}
