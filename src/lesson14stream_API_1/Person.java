package lesson14stream_API_1;

import lesson11Enum.Gender;

public class Person {
	private int id;
    private String name;
	private int age;
	private Gender gender;
	private static int counter = 0;

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
		this.id = counter++;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
        this.age = age;
    }


	@Override
	public String toString() {
		return "Person{ id=" + id +
				", name='" + name + '\'' +
				", age=" + age +
				'}';
	}
}
