package lesson14stream_API_1;


import java.util.ArrayList;
import java.util.List;

public class Solution14_task1 {


    public static void main(String[] args) {
        List<String> products = List.of("молоко", "мед", "мюсли", "сахар", "чай", "сливки", "лимонад");
        System.out.println(countWordsInList(products, 'м'));

    }

    public static int countWordsInList(List<String> list,char ch) {
        return (int) list.stream()
                .filter(s -> s.charAt(0) == ch)
                .peek(System.out::println)
                .count();
    }


}
