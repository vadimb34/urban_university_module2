package lesson15stream_API_2;

import java.util.List;

public class Solution15_task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 1, 2, 3, 1, 2, 3);
        System.out.println(removeDuplicates(list));

        List<String> list2 = List.of("abc", "abc", "rr");
        System.out.println(removeDuplicates(list2));
    }

    public static <T extends Comparable<T>> List<T> removeDuplicates(List<T> list) {
        return list.stream().distinct().toList();
    }
}
