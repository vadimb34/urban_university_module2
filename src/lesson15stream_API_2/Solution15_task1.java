package lesson15stream_API_2;

import java.util.List;

public class Solution15_task1 {

    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println(getSumEvenNumbers(list));
        System.out.println(getSumNotEvenNumbers(list));


    }
    public static int getSumEvenNumbers(List<Integer> list) {
        return list.stream()
                .filter(i -> i % 2 == 0)
                .reduce(0, (x, y) -> x + y);
    }

    public static int getSumNotEvenNumbers(List<Integer> list) {
        return list.stream()
                .filter(i -> i % 2 != 0)
                .reduce(0, (x, y) -> x + y);
    }

}
