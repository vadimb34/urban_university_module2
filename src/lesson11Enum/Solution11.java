package lesson11Enum;

import java.util.ArrayList;
import java.util.List;

public class Solution11 {
    public static void main(String[] args) {
        System.out.println("---------------------------task1---------------------------");
        System.out.println(Alphabet.getNumberLetter(Alphabet.B));
        System.out.println(Alphabet.getNumberLetter(Alphabet.z));
        System.out.println(Alphabet.getNumberLetter("B"));
        System.out.println(Alphabet.getNumberLetter("z"));
        System.out.println("\n---------------------------task2---------------------------");
        List<Person> personList = personGenerator(15);
        System.out.println("All Persons:");
        personList.forEach(System.out::println);
        System.out.println();
        System.out.println("MALE Persons");
        filterPersonList(personList, Gender.MALE).forEach(System.out::println);
    }

    public static List<Person> filterPersonList(List<Person> personList, Gender gender) {
        return personList.stream()
                        .filter(person -> person.getGender().equals(gender))
                        .toList();
    }

    public static List<Person> personGenerator(int count) {
        List<Person> result = new ArrayList<>();
        int age;
        int genderNumber;
        for (int i = 1; i <= count; i++) {
            age  = (int) (Math.random() * 40) + 20;
            genderNumber = (int) (Math.random() * 2) ;
            result.add(new Person("Name" + i, age, Gender.values()[genderNumber]));
        }
        return result;
    }
}
