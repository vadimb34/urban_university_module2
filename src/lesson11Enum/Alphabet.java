package lesson11Enum;

public enum Alphabet {
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z;



    public static int getNumberLetter(Alphabet letter) {
        return letter.ordinal();
    }
    public static int getNumberLetter(String character) {
        return valueOf(character).ordinal();
    }

}
