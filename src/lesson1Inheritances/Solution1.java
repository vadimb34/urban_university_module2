package lesson1Inheritances;

public class Solution1 {

    public static void main(String[] args){
        Footballer footballer1 = new Footballer("Mikle", "sport", 21, 15);
        footballer1.scoredGoal();
        Footballer footballer2 = new Footballer("Jon", "sport", 19, 12);
        footballer2.scoredGoal();
        System.out.println(footballer1);
        System.out.println(footballer2);

        HockeyPlayer hockeyPlayer1 = new HockeyPlayer("Oleg", "sport1", 22);
        hockeyPlayer1.receivedMedal();
        HockeyPlayer hockeyPlayer2  = new HockeyPlayer("Alex",  "sport2",  23);
        hockeyPlayer2.receivedMedal();
        System.out.println(hockeyPlayer1);
        System.out.println(hockeyPlayer2);

        Skier skier1  = new Skier("Serg", "sport3",  25);
        skier1.increaseScore(20);
        Skier skier2  = new Skier("Tom", "sport4",  18);
        System.out.println(skier1);
        System.out.println(skier2);
    }





}
