package lesson1Inheritances;

public class Skier extends Sportsman {

    int score;

    public Skier(String name, String team, int age) {
        super(name, team, age);
    }

    public void increaseScore(int points){
        score += points;
        System.out.println("Skier " + getName() + " increase score by " + points);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    @Override
    public String toString() {
        return "Skier{" +
                "name='" + getName() + '\'' +
                ", team='" + getTeam() + '\'' +
                ", age=" + getAge() + '\'' +
                ", score=" + score +
                '}';
    }



}
