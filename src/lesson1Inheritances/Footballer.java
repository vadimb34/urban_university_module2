package lesson1Inheritances;

public class Footballer extends Sportsman {

    private int numberOfGoalsScored;


    public Footballer(String name, String team, int age, int scoringGoals) {
        super(name, team, age);
        this.numberOfGoalsScored = scoringGoals;
    }


    public void scoredGoal() {
        numberOfGoalsScored++;
        System.out.println("Footballer " + getName() + " scored a goal");
    }

    @Override
    public String toString() {
        return "Footballer{" +
                "name='" + getName() + '\'' +
                ", team='" + getTeam() + '\'' +
                ", age=" + getAge() + '\'' +
                ", numberOfGoalsScored=" + numberOfGoalsScored +
                '}';
    }
}
