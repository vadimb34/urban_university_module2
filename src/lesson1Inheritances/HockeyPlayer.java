package lesson1Inheritances;

public class HockeyPlayer extends Sportsman {

    boolean isReceivedMedal;
    public HockeyPlayer(String name, String team, int age) {
        super(name, team, age);
        isReceivedMedal = false;
    }


    public void receivedMedal() {
        isReceivedMedal = true;
        System.out.println("Hockey player " + getName() + " received medal");
    }

    @Override
    public String toString() {
        return "HockeyPlayer{" +
                "name='" + getName() + '\'' +
                ", team='" + getTeam() + '\'' +
                ", age=" + getAge() + '\'' +
                ", is received medal=" + isReceivedMedal +
                '}';
    }
}
