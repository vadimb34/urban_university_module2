package lesson1Inheritances;
/*
* 1. Создать класс Sportsman с полями строкового типа name, team, поле age – целое число,
* все поля отмечены модификатором private. Создать конструктор класса и методы get для доступа к полям класса.
* В классе есть метод, описывающий общее действие спортсмена.

* Создать классы наследники Footballer, HockeyPlayer, Skier.
* В каждом из них написать свой дополнительный (относящийся к конкретному классу) метод,
* например, у класса Footballer может быть метод scoringGoals, характеризующий результативную игру забиванием голов,
*  у других классов методы будут свои.

В классе с методом main создать экземпляры классов – наследников, по 2 на каждый класс.
* Вывести информацию о каждом экземпляре класса и действие всех имеющихся методов.
 */


public class Sportsman {

    private String name;
    private String team;
    private int age;
    boolean participatesInCompetitions;
    public Sportsman(String name, String team, int age) {
        this.name = name;
        this.team = team;
        this.age = age;
        this.participatesInCompetitions  = false;
    }



    public void startCompetition(){
        participatesInCompetitions = true;
        System.out.println("Спортсмен, " + name + " начал участие в соревнованиях");
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setAme(String ame) {
        this.name = ame;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public boolean isParticipatesInCompetitions() {
        return participatesInCompetitions;
    }

    public void setParticipatesInCompetitions(boolean participatesInCompetitions) {
        this.participatesInCompetitions = participatesInCompetitions;
    }

    @Override
    public String toString() {
        return "Sportsman{" +
                "name='" + name + '\'' +
                ", team='" + team + '\'' +
                ", age=" + age +
                '}';
    }
}
