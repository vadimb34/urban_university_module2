package lesson10Map;

import javax.swing.tree.TreeNode;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Solution10 {
    public static void main(String[] args) {
        System.out.println("---------------------------task1---------------------------");
        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, "a");
        map.put(2, "b");
        map.put(3, "c");
        map.put(4, "d");
        System.out.println("исходный map");
        System.out.println(map);
        System.out.println("набор ключей, содержащихся в  карте");
        System.out.println(map.keySet());
        System.out.println("набор значений, содержащихся в карте");
        System.out.println(map.values());
        System.out.println("набор EnrtySet, содержащихся в карте");
        System.out.println(map.entrySet());

        System.out.println("\n---------------------------task2---------------------------");

        HashMap<Integer, String> map2 = new HashMap<>();
        map2.put(1, "как-так");
        map2.put(2, "нет");
        map2.put(3, "ой");
        map2.put(4, "да");
        map2.put(5, "что-либо");
        System.out.println("исходный Map");
        System.out.println(map2);
        System.out.println("результат");
        System.out.println(task2(map2));
    }

    public static HashMap<Integer, String> task2(HashMap<Integer, String> map) {
        HashMap<Integer, String> result;
        result = (HashMap<Integer, String>) map.keySet().stream()
                .filter(u -> map.get(u).length() > 3)
                .collect(Collectors.toMap(key -> key, key -> map.get(key)));
        return result;
    }
    public static <T> void test(T[] ar1, T[] ar2) {
        System.out.println("---------------------------task3---------------------------");
        System.out.println("исходный массив");
        System.out.println(ar1);
        System.out.println("исходный массив");
        System.out.println(ar2);
        System.out.println("результат");

    }
}
