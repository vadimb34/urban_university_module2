package lesson6FileIO;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution6 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        File file = new File("/home/vadim/UU/txt.txt");
        if (file.exists()) {
            boolean delFileRes = file.delete();
            System.out.println("file " + file.getName() + " has been deleted - " + delFileRes );
        }

        try {
            boolean res = file.createNewFile();
            if  (res) System.out.println("File created - "  + file.getName());
            else {
                System.out.println("Error creating file");
                throw new RuntimeException();
            }
        } catch (IOException e) {
            System.out.println("ошибка создания файла");
            throw new RuntimeException();
        }

        while (true) {
            System.out.println("Введите строку для записи в файл");
            String line = scanner.nextLine();
            if (line.matches("0") || line.equals("exit")) {
                System.out.println("Data has been written in " + file.getName());
                break;
            }
            writeStringToFile(file, line + "\n");
        }
        System.out.println(readDataFromFile(file));
    }

    public static void writeStringToFile(File file, String content) {
        try (FileWriter writer = new FileWriter(file, true)) {
            writer.write(content);
        } catch (Exception e) {
            System.out.println("Error writing to file");
        }
    }

    public static List<String> readDataFromFile(File file) {
        List<String> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file));) {
            while (reader.ready()) {
                list.add(reader.readLine());
            }
        } catch (IOException e) {
            System.out.println("Error reading from file");
        }
        return list;
    }
}
