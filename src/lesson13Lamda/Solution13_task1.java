package lesson13Lamda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Solution13_task1 {
    public static void main(String[] args) {
        BinaryOperator<Integer> operator = (x, y) -> {if ( y % 2 == 0) return (x+y); return x;};
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println(sumOfEven(list, operator, 0));

    }

    public static  Integer sumOfEven(List<Integer> list, BinaryOperator<Integer> operator, Integer identity ) {
        return list.stream().reduce(identity, operator);
    }


}
