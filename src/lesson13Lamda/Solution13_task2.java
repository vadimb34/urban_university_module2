package lesson13Lamda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;

public class Solution13_task2 {
    public static void main(String[] args) {
        System.out.println(findMaxLengthWordInString("один два четыре восемь одиннадцать"));
    }


    public static String findMaxLengthWordInString(String s) {
        Comparator<String> comparator1 = Comparator.comparingInt((x -> x.length()));
//        Comparator<String> comparator = (a, b) -> a.length()- b.length();
        return Arrays.stream(s.split(" "))
                .max(comparator1)
                .orElse("");
    }


}
