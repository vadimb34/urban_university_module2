package lesson12AnonimusClass;

public interface Printable {
    void print(String message);
}
