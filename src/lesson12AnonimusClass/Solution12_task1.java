package lesson12AnonimusClass;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;

public class Solution12_task1 {
    public static void main(String[] args) {
        Printable printToConsole = new Printable() {
            @Override
            public void print(String str) {
                System.out.println(str);
            }
        };
        printToConsole.print("printing to console");

        Printable printToFile = new Printable() {
            @Override
            public void print(String str) {
                try (FileWriter fileWriter = new FileWriter("file.txt")) {
                    fileWriter.write(str);
                    System.out.println("строка '" + str + "' записана в файл " + "file.txt");
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        };
        printToFile.print("print to file");

        Printable printToPanel = new Printable() {
            final JFrame frame = new JFrame("Test print to panel");
            @Override
            public void print(String str) {
                frame.setSize(400, 200);
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                frame.setBounds(screenSize.width/2,screenSize.height/2, 400, 200);
                JPanel panel = new JPanel();
                JTextArea textArea = new JTextArea(str);
                textArea.setFont(new Font("Arial", Font.PLAIN, 30));
                panel.add(textArea);
                frame.add(panel);
                frame.setVisible(true);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        };

        printToPanel.print("Text print on window");

    }
}
