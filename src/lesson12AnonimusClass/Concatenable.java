package lesson12AnonimusClass;

public interface Concatenable <T> {
     String concat(T firstStr, T secondStr);
}
