package lesson12AnonimusClass;

import lesson11Enum.Person;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;

public class Solution12_task2 {
    public static void main(String[] args) {
        Concatenable<String> concatString = new Concatenable<>() {
            @Override
            public String concat(String str1, String str2) {
                return str1 + str2;
            }
        };

        Concatenable<String> crazyConcat = new Concatenable<>() {
            @Override
            public String concat(String str1, String str2)  {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    sb.append(str1).append(str2);
                    System.out.println(sb.toString());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    if (sb.length() > 50) {
                        System.out.println("finish");
                        return sb.toString();
                    }
                }
            }
        };
        System.out.println(concatString.concat("Hello", " World"));
        crazyConcat.concat("1", " 2 ");

        Concatenable<Double> wallet  = new Concatenable<>() {
            @Override
            public String concat(Double startCount, Double deposit) {
                return String.valueOf("balance after transaction: " + (startCount + deposit));
            }
        };

        System.out.println(wallet.concat(234.4, 56.8));

    }


}
