package lesson8ArrayListLinkedList;

import java.util.*;

public class Solution8 {

    public static void main(String[] args) {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("red");
        colors.add("blue");
        colors.add("green");
        colors.add("yellow");
        colors.add("orange");
        colors.add("violet");
        System.out.println("---------------------------------task1---------------------------------");
        System.out.println(colors);
        System.out.println(colors.contains("red"));
        // можно и  так :
        System.out.println(isExistElementInList(colors, "red", new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        }));
        // можно или так
        System.out.println(isExistElementInList(colors, "red"));

        System.out.println("----------------------------------task2------------------------------------");
        LinkedList<Integer> listIntegers = new LinkedList<>();
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        System.out.println(listIntegers);
        System.out.println(swapElementsList(listIntegers));
        System.out.println(swapElementsList(colors));
    }

    public static boolean isExistElementInList(List<String> list, String element) {
        for (String item: list) {
            if (item.equals(element)) {
                return true;
            }
        }
        return false;
    }


    public static <T> boolean isExistElementInList(List<T> list, T element, Comparator<T> comparator) {
        for (T item: list) {
            if (comparator.compare(item, element) == 0) {
                return true;
            }
        }
        return false;
     }


    public static  <T> List<T> swapElementsList(List<T> list)  {
        if (list == null) return null;
        if (list.size() < 2) return list;
        List<T> resList = new LinkedList<>(list);
        int indexStart = 0;
        int indexEnd = list.size() - 1;
        T temp = resList.get(indexEnd);
        resList.set(indexEnd, list.get(indexStart));
        resList.set(indexStart, temp);
        return resList;
    }
}
