package lesson9Sets;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution9 {
    public static void main(String[] args) {
        System.out.println("--------------------task1--------------------");
        HashSet<String> colors1 = new HashSet<>(List.of("red", "blue", "green", "yellow", "orange", "violet"));
        System.out.println("HashSet colors1");
        System.out.println(colors1);
        HashSet<String> colors2 = new HashSet<>(List.of("black", "white", "red", "blue", "green"));
        System.out.println("HashSet colors2");
        System.out.println(colors2);
        System.out.println("общие елементы");
        System.out.println(intersectionOfSets(colors1, colors2));

        System.out.println("--------------------task2--------------------");
        Random random = new Random();
        Set<Integer> integerHashSet = Stream.generate(() -> random.nextInt(40)).limit(10).collect(Collectors.toSet());
        System.out.println("original set");
        System.out.println(integerHashSet);
        System.out.println("result set");
        System.out.println(task2(integerHashSet));
    }

    private static Set<String> task2(Set<Integer> integerHashSet) {
        HashSet<String> res = new HashSet<>();
        for (Integer item : integerHashSet) {
            if (item > 15 && item % 2 == 0) {
                res.add(String.valueOf(item));
            } else  {
                res.add(item /2  + " (origin " + item + ")");
            }
        }
        return res;
    }



    private static <T> Set<T> intersectionOfSets (Set<T> s1, Set<T> s2) {
        Set<T> intersection = new HashSet<>(s1);
        intersection.retainAll(s2);
        return intersection;
    }

}
