package lesson3Interfaces;

public class Solution3 {

    public static void main(String[] args) {
        Bird bird = new Bird();
        Fish fish = new Fish();
        Human human = new Human();

        bird.fly();
        bird.swim();

        fish.swim();
        human.swim();


    }


}
