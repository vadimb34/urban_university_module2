package lesson3Interfaces;

public class Bird  implements Flyable, Swimmable {


    @Override
    public void fly() {
        System.out.println("Bird is flying");
    }

    @Override
    public void swim() {
        System.out.println("Bird is swimming");
    }
}
