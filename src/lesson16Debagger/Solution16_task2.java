package lesson16Debagger;

import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Stream;

public class Solution16_task2 {
    public static void main(String[] args) {
        String str = "Привет, Александр, с праздником, с Новым Годом";
        System.out.println(switchCase(str));
    }

    public static String switchCase(String str) {
        Function<Character, Character> function = u -> {
            String v = String.valueOf(u);
            if (v.matches("[A-ZА-Я]"))
                return v.toLowerCase().charAt(0);
            else if (v.matches("[a-zа-я]"))
                return v.toUpperCase().charAt(0);
            else return u;
        };
        return str.chars()
                .mapToObj(c -> (char) c)
                .map(function)
                .collect(Collector.of(
                    StringBuilder::new,
                    StringBuilder::append,
                    StringBuilder::append,
                    StringBuilder::toString));
    }
}
