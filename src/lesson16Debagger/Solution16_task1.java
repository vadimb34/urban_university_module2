package lesson16Debagger;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Solution16_task1 {
    public static void main(String[] args) {

        Integer[] array = listIntegerGenerator(20);
        System.out.println(Arrays.toString(array));
        System.out.println(getMaxEventNumber(array));
    }




    public static Integer getMaxEventNumber(Integer[] array) {
        Comparator<Integer> comparator  = (i1, i2) -> i2 - i1;
        return Arrays.stream(array).filter(i -> i % 2 == 0).max(Integer::compareTo).orElse(-1);

    }

    public static Integer[] listIntegerGenerator(int count) {
        return Stream.iterate(0, i -> i + 1)
                .limit(count)
                .map((i) -> (int)(Math.random()* 100))
                .toList()
                .toArray(new Integer[0]);
    }


}
