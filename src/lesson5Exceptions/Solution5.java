package lesson5Exceptions;

public class Solution5 {


    public static void main(String[] args) {
        System.out.println("____________________task1--------------------");
        for (int i = 0; i < 10; i++)  {
            int num = (int) (Math.random() * 100);
            System.out.println("threw a random number: " + num);
            try {
                numberIfEven(num);
                System.out.println("this number is even");
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println("____________________task2--------------------");
        try  {
            stringContainsNumber("vsdvsd7vdvsd fwefwfef wefwef");
            System.out.println("string not contains numbers");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }


    public static void numberIfEven(int num) {
        if (num % 2 != 0) {
            throw new IllegalArgumentException("The number is not even");
        }
    }

    public static void stringContainsNumber(String str)  {
        if (str.matches(".*\\d.*"))
            throw new IllegalArgumentException("The string contain number");
    }

}
