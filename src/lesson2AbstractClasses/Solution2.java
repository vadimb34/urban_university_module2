package lesson2AbstractClasses;

public class Solution2 {

    public static void main(String[] args) {
        Guitar guitar = new Guitar();
        guitar.play();
        guitar.tune();

        Piano piano = new Piano();
        piano.play();
        piano.tune();

        Eagle eagle1 = new Eagle(30);
        eagle1.fly();
        eagle1.makeSound();

        Hawk hawk1  = new Hawk(45);
        hawk1.fly();
        hawk1.makeSound();

    }

}
