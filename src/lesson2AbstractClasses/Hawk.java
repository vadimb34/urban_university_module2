package lesson2AbstractClasses;

public class Hawk extends Bird {


    public Hawk(int flySpeed) {
        super(flySpeed);
    }

    @Override
    public void fly() {
        System.out.println("Hawk is flying, his speed is " + getFlySpeed());

    }

    @Override
    public void makeSound() {
        System.out.println("Hawk made a sound");
    }
}
