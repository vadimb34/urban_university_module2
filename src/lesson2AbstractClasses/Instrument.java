package lesson2AbstractClasses;

public abstract class Instrument {

    public abstract void play();
    public abstract void tune();

}
