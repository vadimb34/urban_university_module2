package lesson2AbstractClasses;

public class Eagle extends Bird {


    public Eagle(int flySpeed) {
        super(flySpeed);
    }

    @Override
    public void fly() {
        System.out.println("Eagle is flying, his speed is " + getFlySpeed());

    }

    @Override
    public void makeSound() {
        System.out.println("Eagle made a sound");

    }
}
