package lesson2AbstractClasses;

public abstract class Bird {

    private int flySpeed;


    public Bird(int flySpeed) {
        this.flySpeed = flySpeed;
    }

    public abstract void fly();
    public abstract void makeSound();


    public int getFlySpeed() {
        return flySpeed;
    }

    public void setFlySpeed(int flySpeed) {
        this.flySpeed = flySpeed;
    }


}
