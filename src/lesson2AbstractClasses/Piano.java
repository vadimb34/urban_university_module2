package lesson2AbstractClasses;

public class Piano extends Instrument {

    @Override
    public void play() {
        System.out.println("instrument " + this.getClass().getSimpleName() + " is playing");
    }

    @Override
    public void tune() {
        System.out.println("instrument " + this.getClass().getSimpleName() + " is tuning");
    }

}
