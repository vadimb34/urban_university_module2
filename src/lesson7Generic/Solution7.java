package lesson7Generic;

import java.util.ArrayList;
import java.util.Arrays;

public class Solution7 {

    public static void main(String[] args) {

        System.out.println("-----------------task1-----------------");
        Integer[] a = {1, 2, 3, 4, 5};
        Integer[] b = {1, 2, 3, 4, 6};
        Double[] c = {1.1, 2.2, 3.3, 4.4, 5.5};
        Double[] d = {1.1, 2.2, 3.3, 4.4, 5.5};
        String[] e = {"a", "b", "c"};
        String[] f = {"a", "b", "c1"};
        System.out.println(compareArrays(a, b));
        System.out.println(compareArrays(c, d));
        System.out.println(compareArrays(e, f));
        System.out.println("-----------------task2-----------------");
        Integer[] arInt  = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(Arrays.toString(swapElements(arInt, 0, 4)));
        String[] arString= {"a", "b", "c", "d", "e"};
        System.out.println(Arrays.toString(swapElements(arString, 0, 1)));
    }

    public static <T extends Comparable<T>> boolean  compareArrays(T[] a, T[] b) {
        if(a.length != b.length) { return false;}
        for(int i = 0; i < a.length; i++) {
            if(a[i].compareTo(b[i]) != 0) {
                return false;
            }
        }
        return true;
    }

    public static <T> T[] swapElements(T[] a, int i, int j) {
        if(i < 0 || i >= a.length || j < 0 || j >= a.length) {
            throw new ArrayIndexOutOfBoundsException("Index out of bounds");
        }
        T[] b = Arrays.copyOf(a, a.length);
        if(i == j)  return b;
        T temp = b[i];
        b[i] = b[j];
        b[j] = temp;
        return b;
    }

}
